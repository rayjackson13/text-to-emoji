package meow.rayjackson.texttoemoji;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by rayjackson on 11/10/2016.
 */



public class CustomAdapter extends ArrayAdapter {
    private Context context;
    private int textViewResourceID;
    private String[] args;
    public static boolean flag = false;
    public CustomAdapter(Context context, int textViewResourceID, String[] args){
        super(context, textViewResourceID,  args);
        this.context = context;
        this.textViewResourceID = textViewResourceID;
        this.args = args;
    }

    public View getView(int pos, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView = View.inflate(context, textViewResourceID, null);
        }
        if(flag != false){
            TextView tv = (TextView)convertView;
            tv.setText(args[pos]);
        }
        return convertView;
    }
}
