package meow.rayjackson.texttoemoji;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Random;

import io.github.rockerhieu.emojiconize.Emojiconize;

public class MainActivity extends AppCompatActivity {

    static String[] emojis = { //heart codes
            "\uD83D\uDE02","\uD83D\uDE00", "\uD83D\uDE05", "\uD83D\uDE0D","\uD83D\uDE0B","\uD83D\uDE18",
            "\uD83D\uDE1C", "\uD83D\uDE31","\uD83D\uDE0F","\uD83D\uDE08","\uD83D\uDC4A", "✌️","\uD83D\uDC4C","\uD83D\uDC46",
            "\uD83C\uDF39", "\uD83C\uDF3A","\uD83D\uDD25","\uD83D\uDCA6","☔️", "⚡️","\uD83C\uDF5F","☕️",
            "\uD83C\uDF49", "\uD83C\uDF52","\uD83C\uDF53","\uD83C\uDF77","\uD83C\uDF24", "\uD83C\uDF79",
            "❄️","\uD83C\uDFB2","❤️", "\uD83D\uDC9B","\uD83D\uDC9A","\uD83D\uDC99","\uD83D\uDC9C","\uD83D\uDC95","\uD83D\uDC9E",
            "\uD83D\uDC96","\uD83D\uDC9D","\uD83D\uDC98","\uD83D\uDC9F","♥️","♦️","\uD83C\uDFB4","\uD83C\uDF80",
            "\uD83D\uDCAB","\uD83C\uDF38","\uD83D\uDC33","\uD83D\uDC2C","\uD83D\uDC0B","\uD83D\uDC3E","\uD83C\uDF55",
            "\uD83C\uDF81","\uD83C\uDFEE","\uD83C\uDF88"
    };
    static CheckBox randomBox, manualBox;
    static LinearLayout exampleLayout, spinnerLayout;
    static Spinner s1,s2;
    static EditText input, output, emoji1, emoji2;
    static char[] seq;
    static String face, heart;
    static Button translate;
    final static Random random = new Random();
    static boolean useRandom, useExample;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        exampleLayout = (LinearLayout)findViewById(R.id.emojiExampleLayout);
        spinnerLayout = (LinearLayout)findViewById(R.id.spinnerLayout);

        randomBox = (CheckBox)findViewById(R.id.useRandomEmojis);
        manualBox = (CheckBox)findViewById(R.id.manually);

        emoji1 = (EditText)findViewById(R.id.emojiExample1);
        emoji2 = (EditText)findViewById(R.id.emojiExample2);

        randomBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    exampleLayout.setVisibility(View.GONE);
                    spinnerLayout.setVisibility(View.GONE);
                    manualBox.setVisibility(View.GONE);
                }else{
                    manualBox.setVisibility(View.VISIBLE);
                    spinnerLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        manualBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spinnerLayout.setVisibility(isChecked?View.GONE:View.VISIBLE);
                exampleLayout.setVisibility(isChecked?View.VISIBLE:View.GONE);
                randomBox.setVisibility(isChecked?View.GONE:View.VISIBLE);
            }
        });


        //work with spinners
        s1 = (Spinner)findViewById(R.id.spinner1);
        s2 = (Spinner)findViewById(R.id.spinner2);
        CustomAdapter adapter = new CustomAdapter(this, R.layout.spinner_fragment, emojis);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        s1.setAdapter(adapter);
        s2.setAdapter(adapter);
        AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CustomAdapter.flag = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        s1.setOnItemSelectedListener(onItemSelectedListener);
        s2.setOnItemSelectedListener(onItemSelectedListener);
        //

        input = (EditText)findViewById(R.id.input);
        output = (EditText)findViewById(R.id.result);
        output.setVisibility(View.GONE);
        translate = (Button)findViewById(R.id.translate);
        seq = input.getText().toString().toLowerCase().toCharArray();
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == translate){
                    output.setVisibility(View.VISIBLE);
                    seq = input.getText().toString().toLowerCase().toCharArray();
                    output.setText(makeEmojiWord(seq).toString());
                    ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("none", output.getText());
                    clipboardManager.setPrimaryClip(clip);
                }
            }
        };
        translate.setOnClickListener(clickListener);
    }

    public static String makeEmojiWord(char[] seq) {
        if(randomBox.isChecked()){
            do {
                face = emojis[random.nextInt(emojis.length)];
                heart = emojis[random.nextInt(emojis.length)];
            }while(face == heart);
        }else if(manualBox.isChecked()){
            heart = emoji1.getText().charAt(0) + "";
            face = emoji2.getText().charAt(0) + "";
        }else{
            heart = ""+emojis[s1.getSelectedItemPosition()];
            face = ""+emojis[s2.getSelectedItemPosition()];
        }

        String toAppend = "";
        boolean oneSymbol=false;
        if(seq.length == 1){
            oneSymbol = true;
        }
        for (int i = 0; i<seq.length; i++) {
            switch (seq[i]){
                case 'a':
                    toAppend+=  face+face+heart+heart+heart+"\n"+
                                face+heart+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n";

                    break;
                case 'b':
                    toAppend+=  heart+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+face+"\n";

                    break;
                case 'c':
                    toAppend+=  face+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n"+
                                face+heart+heart+heart+heart+"\n";

                    break;
                case 'd':
                    toAppend+=  heart+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+face+"\n";

                    break;
                case 'e':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n";

                    break;
                case 'f':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n";

                    break;
                case 'g':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n";

                    break;
                case 'h':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n";

                    break;
                case 'i':
                    toAppend+=  face+heart+heart+heart+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+heart+heart+heart+face+"\n";

                    break;
                case 'j':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                face+face+face+heart+face+"\n"+
                                face+face+face+heart+face+"\n"+
                                heart+face+face+heart+face+"\n"+
                                face+heart+heart+face+face+"\n";
                    break;
                case 'k':
                    toAppend+=  heart+face+face+heart+face+"\n"+
                                heart+face+heart+face+face+"\n"+
                                heart+heart+face+face+face+"\n"+
                                heart+face+heart+face+face+"\n"+
                                heart+face+face+heart+face+"\n";
                    break;
                case 'l':
                    toAppend+=  heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+face+"\n";
                    break;
                case 'm':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+heart+face+heart+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'n':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+heart+face+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+face+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'o':
                    toAppend+=  face+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                face+heart+heart+heart+face+"\n";
                    break;
                case 'p':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n";
                    break;
                case 'q':
                    toAppend+=  face+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+face+heart+face+"\n"+
                                face+heart+heart+face+heart+"\n";
                    break;
                case 'r':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+heart+face+face+"\n"+
                                heart+face+face+heart+heart+"\n";
                    break;
                case 's':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;
                case 't':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n";
                    break;
                case 'u':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                face+heart+heart+heart+face+"\n";
                    break;
                case 'v':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                face+heart+face+heart+face+"\n"+
                                face+face+heart+face+face+"\n";
                    break;
                case 'w':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                face+heart+face+heart+face+"\n";
                    break;
                case 'x':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                face+heart+face+heart+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+heart+face+heart+face+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'y':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                face+heart+face+heart+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n";
                    break;
                case 'z':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                face+face+face+heart+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+heart+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;
                case 'а':
                    toAppend+=  face+face+heart+heart+heart+"\n"+
                                face+heart+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'б':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;
                case 'в':
                    toAppend+=  heart+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+face+"\n";
                    break;
                case 'г':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n";
                    break;
                case 'д':
                    toAppend+=  face+face+heart+heart+face+"\n"+
                                face+heart+face+heart+face+"\n"+
                                face+heart+face+heart+face+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'е':
                case 'ё':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;
                case 'ж':
                    toAppend+=  heart+face+heart+face+heart+"\n"+
                                face+heart+heart+heart+face+"\n"+
                                face+heart+heart+heart+face+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n";
                    break;
                case 'з':
                    toAppend+=  heart+heart+heart+heart+face+"\n"+
                                face+face+face+face+heart+"\n"+
                                face+face+heart+heart+face+"\n"+
                                face+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+face+"\n";
                    break;
                case 'и':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+face+face+heart+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+heart+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'й':
                    toAppend+=  face+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+heart+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+heart+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'к':
                    toAppend+=  heart+face+face+heart+face+"\n"+
                            heart+face+heart+face+face+"\n"+
                            heart+heart+face+face+face+"\n"+
                            heart+face+heart+face+face+"\n"+
                            heart+face+face+heart+face+"\n";
                    break;
                case 'л':
                    toAppend+=  face+face+heart+heart+heart+"\n"+
                                face+heart+face+face+heart+"\n"+
                                face+heart+face+face+heart+"\n"+
                                face+heart+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'м':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+heart+face+heart+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'н':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                            heart+face+face+face+heart+"\n"+
                            heart+heart+heart+heart+heart+"\n"+
                            heart+face+face+face+heart+"\n"+
                            heart+face+face+face+heart+"\n";
                    break;
                case 'о':
                    toAppend+=  face+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                face+heart+heart+heart+face+"\n";
                    break;
                case 'п':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n";
                    break;
                case 'р':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                            heart+face+face+face+heart+"\n"+
                            heart+heart+heart+heart+heart+"\n"+
                            heart+face+face+face+face+"\n"+
                            heart+face+face+face+face+"\n";
                    break;
                case 'с':
                    toAppend+=  face+heart+heart+heart+heart+"\n"+
                            heart+face+face+face+face+"\n"+
                            heart+face+face+face+face+"\n"+
                            heart+face+face+face+face+"\n"+
                            face+heart+heart+heart+heart+"\n";
                    break;
                case 'т':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                            face+face+heart+face+face+"\n"+
                            face+face+heart+face+face+"\n"+
                            face+face+heart+face+face+"\n"+
                            face+face+heart+face+face+"\n";
                    break;
                case 'у':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                face+heart+face+heart+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+heart+face+face+face+"\n"+
                                heart+face+face+face+face+"\n";
                    break;
                case 'ф':
                    toAppend+=  face+heart+heart+heart+face+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                face+heart+heart+heart+face+"\n"+
                                face+face+heart+face+face+"\n"+
                                face+face+heart+face+face+"\n";
                    break;
                case 'х':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                            face+heart+face+heart+face+"\n"+
                            face+face+heart+face+face+"\n"+
                            face+heart+face+heart+face+"\n"+
                            heart+face+face+face+heart+"\n";
                    break;
                case 'ц':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n";
                    break;
                case 'ч':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                face+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                face+face+face+face+heart+"\n";
                    break;
                case 'ш':
                    toAppend+=  heart+face+heart+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;

                case 'щ':
                    toAppend+=  heart+face+heart+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n";
                    break;
                case 'ъ':
                    toAppend+=  heart+heart+face+face+face+"\n"+
                                face+heart+face+face+face+"\n"+
                                face+heart+heart+heart+face+"\n"+
                                face+heart+face+face+heart+"\n"+
                                face+heart+heart+heart+face+"\n";
                    break;

                case 'ы':
                    toAppend+=  heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+face+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+heart+face+face+heart+"\n";
                    break;

                case 'ь':
                    toAppend+=  heart+face+face+face+face+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+face+"\n";
                    break;

                case 'э':
                    toAppend+=  heart+heart+heart+heart+face+"\n"+
                                face+face+face+face+heart+"\n"+
                                face+face+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+face+"\n";
                    break;

                case 'ю':
                    toAppend+=  heart+face+heart+heart+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+heart+heart+face+heart+"\n"+
                                heart+face+heart+face+heart+"\n"+
                                heart+face+heart+heart+heart+"\n";
                    break;

                case '1':
                    toAppend+=  face+face+face+face+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                face+face+face+face+heart+"\n";
                    break;


                case '2':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;
                case '3':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;
                case '4':
                    toAppend+=  face+face+heart+heart+face+"\n"+
                                face+heart+face+heart+face+"\n"+
                                heart+face+face+heart+face+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                face+face+face+heart+face+"\n";
                    break;
                case '5':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;
                case '6':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+face+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;
                case '7':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                face+face+face+heart+face+"\n"+
                                face+face+face+heart+face+"\n"+
                                face+face+face+heart+face+"\n";
                    break;
                case '8':
                    toAppend+=  face+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                face+heart+heart+heart+face+"\n"+
                                heart+face+face+face+heart+"\n"+
                                face+heart+heart+heart+face+"\n";
                    break;
                case '9':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n"+
                                face+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;

                case '0':
                    toAppend+=  heart+heart+heart+heart+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+face+face+face+heart+"\n"+
                                heart+heart+heart+heart+heart+"\n";
                    break;
                case 'я':
                    toAppend+=  face+heart+heart+heart+heart+"\n"+
                            heart+face+face+face+heart+"\n"+
                            face+heart+heart+heart+heart+"\n"+
                            face+face+heart+face+heart+"\n"+
                            heart+heart+face+face+heart+"\n";
                    break;
                case ' ':
                    toAppend+=face+face+face+face+face+"\n";
                default:
                    break;

            }
            if(!oneSymbol && !(i == seq.length-1)) {
                toAppend += face + face + face + face + face + "\n";
            }
        }
        return toAppend;
    }
}
